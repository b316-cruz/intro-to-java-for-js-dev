package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        // Create a Scanner object
        Scanner myObj = new Scanner(System.in);
        System.out.print("Enter a username: ");

        // Reading user input
        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);
    }
}
