package com.zuitt.example;

public class Person implements Actions, Greetings {
    public void sleep() {
        System.out.println("Zzz...");
    }

    public void run() {
        System.out.println("Run run run...");
    }

    /* MINI ACTIVITY */
    /*
        Create two new actions that a person would do and implement them in the Actions class. After, run them in the Main class.
    */
    public void talk() {
        System.out.println("Pa, hapapa, hapapatakapa...");
    }

    public void eat() {
        System.out.println("Nyom nyom nyom...");
    }

    public void greet() {
        System.out.println("Henlo!");
    }
    public void bye() {
        System.out.println("Bye~");
    }
}
