package com.zuitt.example;

public class Child extends Parent {
    @Override
    public void speak() {
        System.out.println("I'm a mere child.");
    }
}
