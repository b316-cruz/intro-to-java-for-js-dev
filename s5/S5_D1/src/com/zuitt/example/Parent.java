package com.zuitt.example;

public class Parent {
    /*

    */
    // private properties
    private String name;
    private int age;

    // default constructor
    public Parent() {

    }
    // constructor with arguments (accepting values)
    public Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }
    // methods
    public void greet() {
        System.out.println("Hello friend!");
    }
    public void greet(String name, String timeOfDay) {
        System.out.println("Good " + timeOfDay + ", " + this.name + "!");
    }
    public void speak() {
        System.out.println("I am the parent.");
    }
}
