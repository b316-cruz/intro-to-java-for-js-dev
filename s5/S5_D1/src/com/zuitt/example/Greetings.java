package com.zuitt.example;

public interface Greetings {
    public void greet();
    public void bye();
}
