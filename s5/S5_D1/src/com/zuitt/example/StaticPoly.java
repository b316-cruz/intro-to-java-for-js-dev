package com.zuitt.example;

public class StaticPoly {

    // Static polymorphism: this is the ability to have multiple methods of the same name but changes forms based on the number of arguments or the types of arguments
    public int addition(int a, int b) {
        return a + b;
    }

    // Static polymorphism can be achieved by overloading through changing the number of arguments
    public int addition(int a, int b, int c) {
        return a + b + c;
    }
    // Can also implement overloading by changing the data type of the arguments
    public double addition(double a, double b) {
        return a + b;
    }

    // Another way to implement static polymorphism is through overriding
}
