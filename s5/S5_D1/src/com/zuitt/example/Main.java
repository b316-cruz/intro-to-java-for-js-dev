package com.zuitt.example;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        // Test output
        System.out.println("Hello World");

        // Abstraction
        Person person1 = new Person();

        person1.run();
        person1.sleep();

        person1.talk();
        person1.eat();

        person1.greet();
        person1.bye();

        // Polymorphism
        StaticPoly static1 = new StaticPoly();

        System.out.println(static1.addition(1,5));
        System.out.println(static1.addition(1,5, 10));
        System.out.println(static1.addition(15.5,10.0));

        Parent parent1 = new Parent();
        parent1.speak();

        Child child1 = new Child();
        child1.speak();

        Parent parent2 = new Parent("Sam", 28);
        parent2.greet();
        parent2.greet("Jessie","morning");
    }
}
