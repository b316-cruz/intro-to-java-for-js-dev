package com.zuitt.activity1;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("Jane Smith");
        contact1.setContactNumber("09123456789");
        contact1.setAddress("Quezon City");

        Contact contact2 = new Contact("John Doe", "09021022626", "Caloocan");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            System.out.println("PHONEBOOK CONTACTS:");
            System.out.println("---------------------------");
            for (Contact contact : phonebook.getContacts()) {
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println("---------------------------");
            }
        }
    }
}
