package com.zuitt.activity1;
import java.util.*;

public class Phonebook {
    // instance variable
    private ArrayList<Contact> contacts = new ArrayList<>();

    // default constructor
    public Phonebook() {

    }

    // parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // getters and setters
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }
}
