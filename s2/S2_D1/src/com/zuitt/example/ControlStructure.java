package com.zuitt.example;

public class ControlStructure {
    public static void main(String[] args) {
        // if statements
        int num1 = 10, num2 = 20;
        if(num1 > 5) {
            System.out.println("Num1 is greater than 5.");
        }
        if(num2 > 100) {
            System.out.println("Num2 is greater than 100.");
        } else {
            System.out.println("Num2 is less than 100.");
        }

        // short circuiting through logical operators and if-else statements
        int x = 15, y = 0;
        if (x != 0 || y > 0) {
            System.out.println("Short circuiting works.");
        }

        // switch cases
        int directionValue = 4;
        switch(directionValue) {
            case 1: // a case block within a switch statement. this represents a single case, or a single possible value for the statement
                System.out.println("North");
                break; // the "break" keyword tells that this specific case block has been finished.
                // if there is no proper break statement in a case, the code will bleed over to the next case
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default: // the "default" block handles the scenario if there are no cases that are satisfied
                break;
        }
    }
}
