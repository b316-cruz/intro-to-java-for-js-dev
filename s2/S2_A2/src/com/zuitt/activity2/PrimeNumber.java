package com.zuitt.activity2;

public class PrimeNumber {
    public static void main(String[] args) {
        /* TASK 1 */
        int[] arr_prime = new int[5];

        arr_prime[0] = 2;
        arr_prime[1] = 3;
        arr_prime[2] = 5;
        arr_prime[3] = 7;
        arr_prime[4] = 11;

        System.out.println("The first prime number is: " + arr_prime[0]);
        System.out.println("The second prime number is: " + arr_prime[1]);
        System.out.println("The third prime number is: " + arr_prime[2]);
        System.out.println("The fourth prime number is: " + arr_prime[3]);
        System.out.println("The fifth prime number is: " + arr_prime[4]);

    }
}
