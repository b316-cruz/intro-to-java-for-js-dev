package com.zuitt.activity2;
import java.util.ArrayList;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args) {
        /* TASK 2 */
        ArrayList<String> arr_friends = new ArrayList<>();

        arr_friends.add("John");
        arr_friends.add("Jane");
        arr_friends.add("Chloe");
        arr_friends.add("Zoey");

        System.out.println("My friends are: " + arr_friends);

        /* TASK 3 */
        HashMap<String, Integer> hash_inventory = new HashMap<String, Integer>();

        hash_inventory.put("toothpaste", 15);
        hash_inventory.put("toothbrush", 20);
        hash_inventory.put("soap", 12);

        System.out.print("Our current inventory consists of: " + hash_inventory);
    }
}
