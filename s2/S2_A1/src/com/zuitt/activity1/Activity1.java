package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        int year;
        Scanner objYear = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        year = Integer.parseInt(objYear.nextLine());

        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is  NOT a leap year.");
        }
    }
}
