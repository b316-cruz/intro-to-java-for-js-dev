package com.zuitt.example;

public class Car {
    // properties/attributes: the characteristics of the object the class will create
    // constructor: method to create the object and instantiate with its initialized value
    // getters/setters: are methods to get values of an object's properties or set them
    // methods: actions that an object can perform or do

    // public access: the variable/property in the class is accessible anywhere in the application
    // public: limits the access and ability to get or set a variable/method to only within its own clas
    // getters: methods that return the value of the property
    // setters: methods that allow us to set the value of a property
    private String make;
    private String brand;
    private int price;
    private Driver carDriver;

    // constructor is a method which  allows us to set the initial value of an instance
    public Car() {
        this.carDriver = new Driver();
    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    // getters and setters for our properties
    // getters return a value, therefore, we must add the data type of the value returned
    // Make
    public String getMake() {
        // 'this' keyword refers to the object/insteance where the constructor or setter/getter is
        return this.make;
    }
    public void setMake(String makeParams) {
        this.make = makeParams;
    }
    // Brand
    public String getBrand() {
        return this.brand;
    }
    public void setBrand(String brandParams) {
        this.brand = brandParams;
    }
    // Price
    public int getPrice() {
        return this.price;
    }
    public void setPrice(int priceParams) {
        this.price = priceParams;
    }

    // classes have relationships
    /*
        composition allows modelling objects to be made up of other objects. classes can have instances of other classes.
    */
    public Driver getCarDriver() {
        return this.carDriver;
    }
    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }
    // custom method to retrieve the car driver's name and age
    public String getCarDriverName() {
        return this.carDriver.getName();
    }
    public int getCarDriverAge() {
        return this.carDriver.getAge();
    }
    // methods are functions of an object/instance which allows us to perform certain tasks
    public void start() {
        System.out.println("Vroom vroom...");
    }
}
