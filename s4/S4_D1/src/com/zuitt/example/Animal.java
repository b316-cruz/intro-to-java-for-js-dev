package com.zuitt.example;

public class Animal {
    // private properties
    private String name, color;

    // constructors
    public Animal() {
    }
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    // getters and setters
    public String getName() {
        return this.name;
    }
    public void setName(String nameParams) {
        this.name = nameParams;
    }
    public String getColor() {
        return this.color;
    }
    public void setColor(String colorParams) {
        this.color = colorParams;
    }

    // methods
    public void call() {
        System.out.println("Hi! My name is " + this.name + ".");
    }
}
