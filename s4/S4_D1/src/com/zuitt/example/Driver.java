package com.zuitt.example;

public class Driver {
    private String name;
    private int age;
    public Driver() {

    }
    public Driver(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Name
    public String getName() {
        return this.name;
    }
    public void setName(String nameParams) {
        this.name = nameParams;
    }
    // Age
    public int getAge() {
        return this.age;
    }
    public void setName(int ageParams) {
        this.age = ageParams;
    }

}
