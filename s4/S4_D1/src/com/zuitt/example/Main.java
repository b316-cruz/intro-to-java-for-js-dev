package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        /*System.out.println("Car 1:");
        Car car1 = new Car();
        car1.make = "Veyron";
        car1.brand = "Bugatti";
        car1.price = 200000;
        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println("PhP " + car1.price);

        System.out.println();

        *//* MINI ACTIVITY *//*
        *//*
            Create two new instances of the Car class and save it in a variable called car2 and car3 respectively.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance.
         *//*
        // instance: an object created from a class and each instance should be independent from each other
        System.out.println("Car 2:");
        Car car2 = new Car();
        car2.make = "Forester";
        car2.brand = "Subaru";
        car2.price = 1698000;
        System.out.println(car2.brand);
        System.out.println(car2.make);
        System.out.println("PhP " + car2.price);

        System.out.println();

        System.out.println("Car 3:");
        Car car3 = new Car();
        car3.make = "Silverado";
        car3.brand = "Chevrolet";
        car3.price = 450000;
        System.out.println(car3.brand);
        System.out.println(car3.make);
        System.out.println("PhP " + car3.price);*/

        // instantiating encapsulated Car class
        Car carObj = new Car();

        // property setters
        carObj.setMake("Wigo");
        carObj.setBrand("Toyota");
        carObj.setPrice(729000);

        // property getters
        System.out.println("Make: " + carObj.getMake());
        System.out.println("Brand: " + carObj.getBrand());
        System.out.println("Price: Php " + carObj.getPrice());

        // creating driver instances
        Driver driver1 = new Driver("Alejandro", 25);
        System.out.println("Driver Name: " + driver1.getName());
        System.out.println("Driver Age: " + driver1.getAge());

        // accessing public methods on Car class
        carObj.start();

        // carDriver getters and setters
        Driver newDriver = new Driver("Antonio", 21);
        carObj.setCarDriver(newDriver);
        // get name of new carDriver
        System.out.println("Driver Name: " + carObj.getCarDriver().getName());
        System.out.println("Driver Age: " + carObj.getCarDriver().getAge());

        // using custom methods
        System.out.println("Driver Details: " + carObj.getCarDriverName() + " is " + carObj.getCarDriverAge() + " years old.");

        /* MINI ACTIVITY */
        /*
            Create a new class called Animal with the following attributes
            name - string
            color - string

            Add constructors, getters and setters for the class.
        */
        Animal animalObj = new Animal("", "");
        animalObj.setName("Garfield");
        animalObj.setColor("Orange");
        System.out.println("Name: " + animalObj.getName());
        System.out.println("Color: " + animalObj.getColor());
        animalObj.call();

        // for subclass Dog
        Animal animal1 = new Animal("Clifford", "Red");
        animal1.call();

        Dog dog1 = new Dog();
        dog1.setName("Hachiko");
        dog1.setColor("Brown");
        System.out.println(dog1.getName());
        System.out.println(dog1.getColor());
        dog1.call();

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());
        System.out.println(dog2.getColor());
        System.out.println(dog2.getDogBreed());
        dog2.greet();


    }
}
