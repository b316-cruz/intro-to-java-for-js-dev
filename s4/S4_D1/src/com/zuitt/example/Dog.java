package com.zuitt.example;

public class Dog extends Animal {
    // the 'extends' keyword allows us to have this class inherit the attributes and methods of the Animal class
    // parent class is the class where we inherit from
    // child class/subclass is a class that inherits from a parent
    // no, a subclass cannot inherit from multiple parents. instead, a subclass can "multiple inherit" from what we call interfaces

    // private property (new property for this subclass)
    private String dogBreed;

    // constructor
    public Dog() {
        // the 'super' method inside the subclass constructor is a a reference to the parent class. it is us accessing the constructor method of the parent class
        super();
        // dog breed empty constructor default value
        this.dogBreed = "";
    }
    public Dog(String name, String color, String breed) {
        super(name, color);
        this.dogBreed = breed;
    }

    // getters and setters
    public String getDogBreed() {
        return this.dogBreed;
    }
    public void setDogBreed(String breed) {
        this.dogBreed = breed;
    }

    // method
    public void greet() {
        // a reference to the parent class call method that is being inherited from
        super.call();
        System.out.println("Bark");
    }
}
