package com.zuitt.activity;
import java.util.Date;

public class Course {
    // private properties
    private String name, description;
    private int seats;
    private double fee;
    private Date startDate, endDate;
    private User instructor;

    // constructors
    public Course() {}
    public Course(String name, String description, int seats, double fee, Date startDate, Date endDate, User instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    // getters and setters
    // get
    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }
    public int getSeats() {
        return this.seats;
    }
    public double getFee() {
        return this.fee;
    }
    public Date getStartDate() {
        return this.startDate;
    }
    public Date getEndDate() {
        return this.endDate;
    }
    // set
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setSeats(int seats) {
        this.seats = seats;
    }
    public void setFee(double fee) {
        this.fee = fee;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    // get-set for User instructor
    public String getInstructorName() {
        return this.instructor.getName();
    }
    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }
}
