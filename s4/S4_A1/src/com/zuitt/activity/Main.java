package com.zuitt.activity;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        // instantiation of User class
        User userObj = new User("Terence Gaffud", "tgaff@mail.com", "Quezon City", 25);

        // instantiation of Course class
        Course courseObj = new Course();
        courseObj.setName("MACQ004");
        courseObj.setDescription("An introduction to Java for career-shifters");
        courseObj.setSeats(30);
        courseObj.setFee(8000);
        courseObj.setStartDate(new Date());
        courseObj.setEndDate(new Date());
        courseObj.setInstructor(userObj);

        // displaying messages
        System.out.println("Hi! I'm " + userObj.getName() + ". I'm " + userObj.getAge() + " years old. You can reach me via my email: " + userObj.getEmail() + ". When I'm off at work, I can be found at my house in " + userObj.getAddress() + ".");
        System.out.println();
        System.out.println("Welcome to the course " + courseObj.getName() + ". This course can be described as "  + courseObj.getDescription() + ". Your instructor for this course is Ma'am/Sir "  + courseObj.getInstructorName() + ". Enjoy!");
    }
}
