package com.zuitt.activity;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        /* MAIN ACTIVITY */
        Scanner in = new Scanner(System.in);
        int num = 0;
        try {
            System.out.println("Input an integer whose factorial will be computed:");
            num = in.nextInt();
            int answer = 1, count = 1;
            if(num != 0) {
                if(num > 0) {
                    while(count <= num){
                        answer = answer * count;
                        count++;
                    }
                    System.out.println("Using, while loop, the factorial of " + num + " is " + answer + ".");

                    for(; count <= num; count++){
                        answer = answer * count;
                    }
                    System.out.println("Using, for loop, the factorial of " + num + " is " + answer + ".");
                } else {
                    System.out.println("The factorial of negative numbers is undefined.");
                }
            } else {
                System.out.println("The factorial of 0 is 1.");
            }
        } catch (Exception e) {
            System.out.println("Invalid input.");
            e.printStackTrace();
        }

        /* STRETCH GOAL*/
        System.out.println();
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
