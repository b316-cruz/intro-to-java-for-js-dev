package com.zuitt.example;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        // The Scanner object looks for which input to scan. In this case, it is the console(System.in)
        Scanner input = new Scanner(System.in);
        // The Scanner object has multiple methods that can be used to get the expected value
        /*
            nextInt() - expects an integer
            nextDouble() - expects a double
            nextLine() - gets the entire line as a String
        */
        System.out.println("Input a number: ");

        int num = 0;
        try { // try to do this statement
            // The nextInt() method tells Java that it is expecting an integer value as input
            num = input.nextInt();
            System.out.println("You have entered: " + num);
        } catch (Exception e) { // catch any errors
            System.out.println("Invalid input");
            e.printStackTrace(); // pinpoints where or which line the error is
        }
    }
}
